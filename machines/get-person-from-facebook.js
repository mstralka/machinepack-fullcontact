module.exports = {
  friendlyName: 'Get Person from Facebook',
  description: 'Retrieve more information about a specific person by facebook username or facebook ID',
  extendedDescription: '',
  inputs: {
    apiKey: {
      required: true,
      example: '00011122233344455',
      description: 'Your FullContact API key',
      whereToGet: {
        url: 'https://www.fullcontact.com/developer/',
        description: 'Sign up for a free FullContact developer account here: https://www.fullcontact.com/developer/try-fullcontact/',
        extendedDescription: ''
      }
    },
    username: {
      example: 'janedoe',
      description: 'The facebook name of the person being looked up',
      extendedDescription: 'facebookUsername takes precedence over facebookId if both are specified.'
    },
    id: {
      example: '123456789',
      description: 'The facebook ID of the person being looked up',
      extendedDescription: 'facebookUsername takes precedence over facebookId if both are specified.'
    },
    queue: {
      example: '1',
      description: 'Using this parameter notifies FullContact that the email address in question will be called later. It allows the API to make sure it has indexed the email address prior to the application needing it. When using this parameter, it will always return a response code of 202.'
    },
    webhookUrl: {
      example: 'https://mydomain.com/callback/listener',
      description: 'The callback url you\'d like the data to be posted back to.',
      extendedDescription: 'Read the webhook flow diagram: https://www.fullcontact.com/developer/docs/person/#webhook-flow-diagram'
    },
    webhookId: {
      example: 'myId',
      description: 'You can enter anything you want here, we will just pass it back in the response. This allows you to track the webhook if you wish.'
    }
  },
  defaultExit: 'success',
  exits: {
    // Status 5xx
    error: {
      description: 'FullContact says something is wrong with their system.  It may be temporary or you may want to contact FullContact support.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 400
    malformedRequest: {
      description: 'FullContact says your API request was malformed',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 403
    apiKeyProblem: {
      description: 'FulLContact says your API key is invalid or your rate limit has been reached.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 422
    queryParamProblem: {
      description: 'FullContact says you have an invalid or missing query parameter.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 202
    queued: {
      description: 'FullContact has queued the search and will have an answer shortly.  If you don\'t want to wait, use the webhook input',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 404
    wait24Hours: {
      description: 'FullContact says you have to wait 24 hours to try this query again.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 200
    success: {
      description: 'Done.'
    }
  },
  fn: function(inputs, exits) {
    var FullContact = require('fullcontact');
    var fullcontact = new FullContact(inputs.apiKey);

    var facebookUsername = inputs.username;
    var facebookId = inputs.id;
    var queue = (inputs.queue && inputs.queue === '1') ? 1 : 0;
    var webhookUrl = inputs.webhookUrl || null;
    var webhookId = inputs.webhookId || null;

    // Username takes precedence
    if (facebookUsername) {
      fullcontact.person.facebook(facebookUsername, queue, webhookUrl, webhookId, function(err, data) {
        if (err) {
          if (202 === err.status) {
            return exits.queued(err);
          } else if (404 === err.status) {
            return exits.wait24Hours(err);
          } else if (400 === err.status) {
            return exits.malformedRequest(err);
          } else if (403 === err.status) {
            return exits.apiKeyProblem(err);
          } else if (422 === err.status) {
            return exits.queryParamProblem(err);
          }
          return exits.error(err);
        }
        return exits.success(data);
      });
    } else if (facebookId) {
      fullcontact.person.facebookId(facebookId, function(err, data) {
        if (err) {
          if (404 === err.status) {
            return exits.wait24Hours(err);
          } else if (400 === err.status) {
            return exits.malformedRequest(err);
          } else if (403 === err.status) {
            return exits.apiKeyProblem(err);
          } else if (422 === err.status) {
            return exits.queryParamProblem(err);
          }
          return exits.error(err);
        }
        if (202 === data.status) {
          return exits.queued(data);
        }
        return exits.success(data);
      });
    } else {
      return exits.missingArgument('username or id');
    }

  }
};
