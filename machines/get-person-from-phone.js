module.exports = {
  friendlyName: 'Get Person from Phone',
  description: 'Retrieves contact information by phone number.',
  extendedDescription: 'Read the docs here: https://www.fullcontact.com/developer/docs/person/',
  inputs: {
    apiKey: {
      required: true,
      example: '00011122233344455',
      description: 'Your FullContact API key',
      whereToGet: {
        url: 'https://www.fullcontact.com/developer/',
        description: 'Sign up for a free FullContact developer account here: https://www.fullcontact.com/developer/try-fullcontact/',
        extendedDescription: ''
      }
    },
    phone: {
      required: true,
      example: '123-555-4444',
      description: 'The phone number of the person you are looking up'
    }
  },
  defaultExit: 'success',
  exits: {
    // Status 5xx
    error: {
      description: 'FullContact says something is wrong with their system.  It may be temporary or you may want to contact FullContact support.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 400
    malformedRequest: {
      description: 'FullContact says your API request was malformed',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 403
    apiKeyProblem: {
      description: 'FulLContact says your API key is invalid or your rate limit has been reached.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 422
    queryParamProblem: {
      description: 'FullContact says you have an invalid or missing query parameter.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 202
    queued: {
      description: 'FullContact has queued the search and will have an answer shortly.  If you don\'t want to wait, use the webhook input',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 404
    wait24Hours: {
      description: 'FullContact says you have to wait 24 hours to try this query again.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 200
    success: {
      description: 'Done.'
    }
  },
  fn: function(inputs, exits) {
    var FullContact = require('fullcontact');
    var fullcontact = new FullContact(inputs.apiKey);

    fullcontact.person.phone(inputs.phone, function(err, data) {
      if (err) {
        if (404 === err.status) {
          return exits.wait24Hours(err);
        } else if (400 === err.status) {
          return exits.malformedRequest(err);
        } else if (403 === err.status) {
          return exits.apiKeyProblem(err);
        } else if (422 === err.status) {
          return exits.queryParamProblem(err);
        }
        return exits.error(err);
      }
      if (202 === data.status) {
        return exits.queued(data);
      }
      return exits.success(data);
    });
  }
};
