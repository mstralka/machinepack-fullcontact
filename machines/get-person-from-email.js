module.exports = {
  friendlyName: 'Get Person from Email',
  description: 'Retrieves contact information by e-mail address.',
  extendedDescription: 'Read the docs here: https://www.fullcontact.com/developer/docs/person/',
  inputs: {
    apiKey: {
      required: true,
      example: '00011122233344455',
      description: 'Your FullContact API key',
      whereToGet: {
        url: 'https://www.fullcontact.com/developer/',
        description: '',
        extendedDescription: ''
      }
    },
    email: {
      required: true,
      example: 'jane@example.com',
      description: 'The email address of the person you are looking up'
    },
    queue: {
      example: '1',
      description: 'Using this parameter notifies FullContact that the email address in question will be called later. It allows the API to make sure it has indexed the email address prior to the application needing it. When using this parameter, it will always return a response code of 202.'
    },
    webhookUrl: {
      example: 'https://mydomain.com/callback/listener',
      description: 'The callback url you\'d like the data to be posted back to.',
      extendedDescription: 'Read the webhook flow diagram: https://www.fullcontact.com/developer/docs/person/#webhook-flow-diagram'
    },
    webhookId: {
      example: 'myId',
      description: 'You can enter anything you want here, we will just pass it back in the response. This allows you to track the webhook if you wish.'
    },
    webhookBody: {
      example: 'json',
      description: 'You can specify that the payload of the webhook response be retuned as a JSON document instead of a URL-encoded form using the webhookBody=json query parameter. A JSON document will be the format regardless of whether the initial request to v2/Person was to the .json, .xml or .html version of the person endpoint.'
    }
  },
  defaultExit: 'success',
  exits: {
    // Status 5xx
    error: {
      description: 'FullContact says something is wrong with their system.  It may be temporary or you may want to contact FullContact support.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 400
    malformedRequest: {
      description: 'FullContact says your API request was malformed',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 403
    apiKeyProblem: {
      description: 'FulLContact says your API key is invalid or your rate limit has been reached.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 422
    queryParamProblem: {
      description: 'FullContact says you have an invalid or missing query parameter.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 202
    queued: {
      description: 'FullContact has queued the search and will have an answer shortly.  If you don\'t want to wait, use the webhook input',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 404
    wait24Hours: {
      description: 'FullContact says you have to wait 24 hours to try this query again.',
      extendedDescription: 'Refer to the flow diagram: https://www.fullcontact.com/developer/docs/person/#flow-diagram'
    },
    // Status 200
    success: {
      description: 'Done.'
    }
  },
  fn: function(inputs, exits) {
    var FullContact = require('fullcontact');
    var fullcontact = new FullContact(inputs.apiKey);

    var email = inputs.email.toLowerCase();
    var queue = (inputs.queue && inputs.queue === '1') ? 1 : 0;
    var webhookUrl = inputs.webhookUrl || null;
    var webhookId = inputs.webhookId || null;
    var webhookBody = inputs.webhookBody || null;

    fullcontact.person.email(email, queue, webhookUrl, webhookId, webhookBody, function(err, data) {
      if (err) {
        if (404 === err.status) {
          return exits.wait24Hours(err);
        } else if (400 === err.status) {
          return exits.malformedRequest(err);
        } else if (403 === err.status) {
          return exits.apiKeyProblem(err);
        } else if (422 === err.status) {
          return exits.queryParamProblem(err);
        }
        return exits.error(err);
      }
      if (202 === data.status) {
        return exits.queued(data);
      }
      return exits.success(data);
    });
  }
};
